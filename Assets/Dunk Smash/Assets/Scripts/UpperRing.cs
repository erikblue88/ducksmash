﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpperRing : MonoBehaviour {

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            //if the player collides with the top of the ring first, only then can score for a limited time
            //this way we avoid scoring from under the ring
            CanScore();
        }
    }

    public void CanScore()
    {
        FindObjectOfType<Player>().noScore = false;
        Invoke("NoScore", 1f);
    }

    public void NoScore()
    {
        if (!(IsInvoking("NoScore")))
            FindObjectOfType<Player>().noScore = true;
    }
}
