﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public ParticleSystem jumpEffect;
    public float linearSpeed, verticalSpeed, rotationSpeed, rotationValue;
    public bool canTeleport = true, noScore = true;

    private Rigidbody2D rb;
    private Quaternion rotateTo;

	void Start () {
        rb = GetComponent<Rigidbody2D>();
        linearSpeed *= -1f;     //to make the player move right first
    }

	void Update () {
        if ((Input.GetMouseButtonDown(0)) && (FindObjectOfType<ProcessBar>().gameOver == false))
        {
            FindObjectOfType<AudioManager>().JumpSound();
            rb.velocity = Vector3.zero;     //'freezes' the player
            if(rb.velocity.y < 2f)      //does not allow 'overpowering'
                rb.AddForce(Vector2.up * verticalSpeed);       //jump
            rb.AddForce(Vector2.right * linearSpeed, ForceMode2D.Impulse);     //adds the left/right movement
            jumpEffect.Play();      //selected particle will be player
        }

        transform.Rotate(Vector3.forward, rotationSpeed * -rb.velocity.x * Time.deltaTime);     //rotates the player towards the direction it goes (left/right)
    }
}
