﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ProcessBar : MonoBehaviour {

    public Image processBarImage;
    public float minTime, timeStep, firstTime;
    public bool gameOver = false;

    private float timerEnd, timeSinceStart;
    private bool firstShot = true;

    void Start()
    {
        timerEnd = firstTime;
    }

    public void SetTimer()
    {
        firstShot = false;
        timeSinceStart = 0f;
        ReduceTimer();
    }

    public void ReduceTimer()
    {
        if(timerEnd >= minTime)
            timerEnd -= timeStep;
    }

    void LateUpdate()
    {
        if (!firstShot)     //at the first shot we don't count down
        {
            timeSinceStart += Time.deltaTime;       //time since the countdown's start is increasing continously
            processBarImage.fillAmount = 1 - (1f * (timeSinceStart / timerEnd));        //sets the fill amount of the process bar
            if (processBarImage.fillAmount == 0)        //if the timer counted down
            {//then game is over
                gameOver = true;
                FindObjectOfType<Ring>().enabled = false;
                FindObjectOfType<Spawner>().enabled = false;
                FindObjectOfType<GameManager>().EndPanelActivation();
                FindObjectOfType<GameManager>().HighScoreCheck();
            }
        }
    }
}
