﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shadow : MonoBehaviour {

    public GameObject player;
    public float maxX, maxY, minX, minY, scaleSpeed;

    private Vector2 playerPos;
    private Vector3 scale;

    void Update () {
        playerPos = player.transform.position;
        playerPos.y = transform.position.y;
        transform.position = playerPos;

        scale.x = maxX - (player.transform.position.y + 3.58f) * scaleSpeed * 1.5f;
        scale.y = maxY - (player.transform.position.y + 3.58f) * scaleSpeed;

        if (scale.x < minX)
            scale.x = minX;
        if (scale.y < minY)
            scale.y = minY;
        if (scale.x > maxX)
            scale.x = maxX;
        if (scale.y > maxY)
            scale.y = maxY;

        transform.localScale = scale;
	}
}
