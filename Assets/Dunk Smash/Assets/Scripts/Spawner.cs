﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject leftBasket, rightBasket;
    public Transform[] leftSpawnPoints, rightSpawnPoints;
    public bool rightSide = true;

    private GameObject actBasket;

    public void Spawn()
    {
        if (actBasket != null)  //if there is a basket
        {
            //destroying actual basket
            if(actBasket.tag == "LeftBasket")       //if it's 'leftbasket'
                actBasket.transform.GetChild(0).GetComponent<Animation>().Play("BasketEndLeft");        
            else       //if it's 'rightbasket'
                actBasket.transform.GetChild(0).GetComponent<Animation>().Play("BasketEndRight");
            Destroy(actBasket, 1f);
        }

        if (!rightSide)
        {
            //spawns basket, sets the next basket's side
            actBasket = Instantiate(leftBasket, leftSpawnPoints[Random.Range(0, leftSpawnPoints.Length)].position, Quaternion.identity);
            rightSide = true;
        }
        else
        {
            //spawns basket, sets the next basket's side
            actBasket = Instantiate(rightBasket, rightSpawnPoints[Random.Range(0, rightSpawnPoints.Length)].position, Quaternion.identity);
            rightSide = false;
        }
    }
}
