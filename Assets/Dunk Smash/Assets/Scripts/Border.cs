﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Border : MonoBehaviour {

    private Vector2 leftBorderPos, rightBorderPos;

    void Start()
    {//gets the borders' position
        leftBorderPos = GameObject.FindGameObjectWithTag("LeftBorder").transform.position;    
        rightBorderPos = GameObject.FindGameObjectWithTag("RightBorder").transform.position;    
    }

    private void OnTriggerStay2D(Collider2D collision)
    {//moves the player toward the other direction (if it's right then it will move left vice versa)
        if (collision.tag == "Player")
        {
            if (FindObjectOfType<Player>().canTeleport)
            {
                FindObjectOfType<Player>().canTeleport = false;
                Invoke("CanTeleport", 1f);      //canTeleport will be set to true again

                if (gameObject.tag == "RightBorder")
                {
                    leftBorderPos.y = collision.gameObject.transform.position.y;    //to maintain the position on Y
                    collision.GetComponent<Transform>().position = leftBorderPos;
                }
                else
                {
                    rightBorderPos.y = collision.gameObject.transform.position.y;   //to maintain the position on Y
                    collision.GetComponent<Transform>().position = rightBorderPos;
                }
            }
        }
    }

    public void CanTeleport()
    {
        FindObjectOfType<Player>().canTeleport = true;
    }
}
