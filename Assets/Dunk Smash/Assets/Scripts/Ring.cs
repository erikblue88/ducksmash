﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ring : MonoBehaviour {

    public GameObject ringEffect;

    private bool scored = false;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {//if the player collides with the middle of the ring
            if((!(FindObjectOfType<Player>().noScore)) && (!scored) && (FindObjectOfType<ProcessBar>().gameOver==false))
            {
                Destroy(Instantiate(ringEffect, transform.position, Quaternion.identity), 1f);      //spawns 'ringEffect' to the position of the gameObject and destroys it after x seconds
                Score();
            }
        }
    }

    public void Score()
    {
        scored = true;
        FindObjectOfType<Score>().IncreaseScore();
        FindObjectOfType<Player>().linearSpeed *= -1f;      //if the player was moving right, it will move left, vice versa
        FindObjectOfType<ProcessBar>().SetTimer();      //sets the timer on processbar
        FindObjectOfType<Spawner>().Invoke("Spawn", 0.5f);      //spawns basket
    }
}
