﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Score : MonoBehaviour {

    public TextMeshProUGUI scoreText;
    public int score = 0;

    private Animation camAnim;

    private void Start()
    {
        camAnim = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Animation>();
    }

    public void IncreaseScore()
    {
        FindObjectOfType<AudioManager>().ScoreSound();
        //camAnim.Play();       //If you would like to use the zoomInAnim, then uncomment this line
        scoreText.text = (++score).ToString();
    }
}
